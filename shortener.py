import webapp
import shelve
import random
from urllib.parse import unquote
import string
import os
import platform

dataBase = shelve.open('urlsRandomshort')
lista = ""

FORM = """
    
    <form action ="" method ="POST">
     URL a acortar:<br>
    <input type="text" name="url"><br><br>
    <input type="submit" value="Enviar"><br>
 </form>
"""



PAGE_redirect = """<html>
<body>
<h2>App para acortar URLs</h2>
<br>
<h3>""" + FORM + """</h3>
<br>  
Listado de URLs acortadas: 
<ul>
""" + lista + """
</ul>
</body>
</html>"""



PAGE_NOT_FOUND = """
<html>
<body>
<p>Recurso no encontrado</p>
""" + FORM + """  
</body>
</html>
"""

PAGE_NOT_EXIST = """
<html>
<body>
<p>Pagina solicitada no existe </p>
""" + FORM + """  
</body>
</html>
"""
def ping(url):
  """
  Hace ping a una página web para verificar conectividad.
  Devuelve True si la página responde, False en caso contrario
  """

  param = '-n' if platform.system().lower()=='windows' else '-c'

  # Construye el comando ping
  command = ['ping', param, '1', url]

  return os.system(' '.join(command)) == 0

class rSApp (webapp.WebApp):

    def parse(self, request):
        if request == "":  # Para evitar el error cuando llega un solo elemento vacio
            return 'None'
        else:
            # Tras dividir la cadena selecciono el segundo elemento de la lista

            url = request.splitlines()# Divido la cadena en líneas
            print(url)
            firstLineWords = url[0].split(' ',2)# Divido la primera línea en palabras
            lastLineWords = url[-1].split(' ',2)# Divido la última línea en palabras la url
            print(lastLineWords)
            method = firstLineWords[0]# Obtengo el método
            resource = firstLineWords[1]# Obtengo el recurso
            body=lastLineWords[0]# Obtengo el cuerpo
            print('esto es el metodo: ' + method + '\n' ,'esto es el recurso : ' + resource+ '\n', 'esto es el body : ' + body+ '\n')

        return method, resource,body

    def process(self, reqParsed):

        method,resource,body=reqParsed

        if method == "GET":                                   # GET /

            if resource == "/":
                listaUrls = ""
                for key, value in dataBase.items():
                    listaUrls += "<li>" + "<a href=http://localhost:1234/" + str(value) + ">" + \
                                 str(key) + "</a>: " + value + "</li>"
                htmlAnswer = FORM + "<br><br><h3>Listado de URLs acortadas: </h3><ul>" + listaUrls + "</ul>"
                httpCode = "200 OK"

            elif resource == "/favicon.ico":
                httpCode = "404 Not Found"
                htmlAnswer = PAGE_NOT_FOUND

            else:                                            # GET /recurso

                url_code = str(resource.split("/")[1])

                if url_code in dataBase.keys():

                    print('REDIRECION A : ' + str(dataBase[url_code]))
                    httpCode = "302 Found"
                    htmlAnswer = ("<html><head>" + "<meta http-equiv='refresh' content='3; URL=" +
                                   dataBase[url_code] + "'></head><body><h2> moviendo a " +
                                   "<a href=" + dataBase[url_code] + ">" + str(dataBase[url_code]) +
                                   "</a></h2>" + "</body></html>")
                else:
                    httpCode = "404 Not Found"
                    htmlAnswer = PAGE_NOT_FOUND





        elif method == "POST":
            if (body == "url=") or (body == ""):  #cuerpo vacio

                httpCode = "400 BAD REQUEST"
                htmlAnswer = PAGE_NOT_FOUND

            else:
                urlBody = unquote(body.split("=")[1])  # cuerpo de la peticion con url,
                if ping(urlBody):
                    # controlo si el url es http o https y lo formateo
                    if urlBody.startswith(("https://www.", "http://www.")):
                        urlBody = "http://" + urlBody.split(".")[1] + "." + urlBody.split(".")[2]
                    elif urlBody.startswith(("http://","https://" )):
                        urlBody = urlBody
                    else:
                        urlBody = "https://" + urlBody
                    if not urlBody in dataBase.values():
                        randomURL = ''.join(random.choices(string.ascii_lowercase + string.digits, k=6))
                        if randomURL in dataBase.keys():
                            randomURL = ''.join(random.choices(string.ascii_lowercase + string.digits, k=6))

                        dataBase[randomURL] = urlBody
                    else :
                        pass

                    httpCode = "200  OK"
                    listaUrls = ""
                    for key, value in dataBase.items():
                        listaUrls += "<li>" + "<a href=http://localhost:1234/" + str(key) + ">" + \
                                 str(key) + "</a>: " + value + "</li>"
                    htmlAnswer = FORM + "<br><br><h3>Listado de URLs acortadas: </h3><ul>" + listaUrls + "</ul>"
                else:
                    listaUrls = ""
                    for key, value in dataBase.items():
                        listaUrls += "<li>" + "<a href=http://localhost:1234/" + str(key) + ">" + \
                                     str(key) + "</a>: " + value + "</li>"
                    htmlAnswer = FORM + "<br><br><h3>Listado de URLs acortadas: </h3><ul>" + listaUrls + "</ul>"
                    httpCode = "400 BAD REQUEST"
                    htmlAnswer = PAGE_NOT_EXIST + listaUrls



        return (httpCode, htmlAnswer )

if __name__ == "__main__":
    test = rSApp ("localhost", 1234)